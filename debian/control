Source: golang-github-kurin-blazer
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Michael Stapelberg <stapelberg@debian.org>,
           Félix Sipma <felix+debian@gueux.org>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-google-subcommands-dev,
               golang-github-google-uuid-dev,
               golang-github-grpc-ecosystem-grpc-gateway-dev,
               golang-golang-x-net-dev,
               golang-google-genproto-dev,
               golang-google-grpc-dev,
               golang-goprotobuf-dev
Standards-Version: 4.6.1
Homepage: https://github.com/kurin/blazer
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-kurin-blazer
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-kurin-blazer.git
XS-Go-Import-Path: github.com/kurin/blazer
Testsuite: autopkgtest-pkg-go
Rules-Requires-Root: no

Package: golang-github-kurin-blazer-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         golang-github-google-subcommands-dev,
         golang-github-google-uuid-dev,
         golang-github-grpc-ecosystem-grpc-gateway-dev,
         golang-golang-x-net-dev,
         golang-google-genproto-dev,
         golang-google-grpc-dev,
         golang-goprotobuf-dev
Description: Go library for Backblaze's B2
 Blazer is a Golang client library for Backblaze's B2 object storage
 service.  It is designed for simple integration with existing applications
 that may already be using S3 and Google Cloud Storage, by exporting only
 a few standard Go types.
 .
 It implements and satisfies the B2 integration checklist
 (https://www.backblaze.com/b2/docs/integration_checklist.html),
 automatically handling error recovery, reauthentication, and other
 low-level aspects, making it suitable to upload very large files, or
 over multi-day time scales.
